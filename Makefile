.PHONY: up down

COMPOSE := docker-compose

up:
	DEBEZIUM_VERSION=2.4 $(COMPOSE) up -d

down:
	DEBEZIUM_VERSION=2.4 $(COMPOSE) kill
